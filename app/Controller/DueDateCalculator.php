<?php


namespace App\Controller;

use App\Exception\DueDateCalculatorException;

use App\Controller\DueDateCalculatorValidator;

use Datetime;

class DueDateCalculator {

    private $_startWorkingHour;
    private $_endWorkingHour;
    private $_validator;

    public function __construct($startWorkingHour, $endWorkingHour) {

        $this->_startWorkingHour = $startWorkingHour;
        $this->_endWorkingHour = $endWorkingHour;

        $this->_validator = new DueDateCalculatorValidator();

    }

    /**
     * @param submitDate, the date and time of issue report
     * @param turnaround, the accepted working hours of issue
     * @throws DueDateCalculatorException, when the paramteres are empty or not valid format
     * @throws DueDateCalculatorException, when the submitDate is not between the start and end working hours and it is not on a Workday
     * @return the calculated date when the issue should be solved
     */
    public function calculateDueDate($submitDate, $turnaround) {

        if (!$this->_validator->calculateParamsAreValid($submitDate, $turnaround)) {
            throw new DueDateCalculatorException("The parameters are not valid");
        }

        if (!$this->_validator->submitDateIsValid($submitDate, $this->_startWorkingHour, $this->_endWorkingHour)) {
            throw new DueDateCalculatorException("You can report issues only between 9AM and 5pm on Workdays!");
        }

        $minutes = $turnaround * 60;
        $endDate = $this->_calculateTime($submitDate, $minutes);
        return $endDate;

    }

    public function getStartWorkingHour() {
        return $this->_startWorkingHour;
    }

    public function getEndWorkingHour() {
        return $this->_endWorkingHour;
    }

    /**
     * Add the time to the submitted date 
     * Have to deal with the time and day overwflow 
     * Set the end date of the issue
     * @param date, the date which will be the base of the sum
     * @param addMinutes, how many minutes left
    */
    private function _calculateTime($date, $minutes) {
        $currentTime = $date;
        $addMinutes =$minutes;
        $currentDay = (int) date("d", strtotime($currentTime));
        if ($addMinutes) {
            $newTime = date("H:i", strtotime($currentTime . " + " . $addMinutes . " minutes"));
            $newDate = date("Y-m-d H:i", strtotime($currentTime . " + " . $addMinutes . " minutes"));
        }
        $newDay = (int) date("d", strtotime($newDate));
        if (!$this->_validator->submitDateIsValid($newDate, $this->_startWorkingHour, $this->_endWorkingHour) || $currentDay !== $newDay) {
            $nextDay = $this->_getNextWorkDay($currentTime);
            $addMinutes = $addMinutes - $this->_getUsedTime($currentTime);
            $newDate = $this->_calculateTime($nextDay,$addMinutes);
        }
        
        return $newDate;

    }

    /**
     * Get the next working day 
     * Handle the weekend
     * @param date, the start date
     * @return the number of the next working day
    */
    private function _getNextWorkDay($date) {
        $nextDay = date("Y-m-d", strtotime($date . " + 1 days"));
        $nextDayNumber = (int) date('N', strtotime($nextDay));
        if ($nextDayNumber >= 1 && $nextDayNumber < 6) {
           return $nextDay . " " . (string) $this->_startWorkingHour . ":00" ;
        }
        return $this->_getNextWorkDay($nextDay);
    }

    /**
     * Calculate the used time from the submitted date
     * @param date, the start date (the left date)
     * @return the calculated minutes
    */
    private function _getUsedTime($date) {
        $startDate = date('Y-m-d',strtotime($date)) . " " . (string) $this->_endWorkingHour . ":00";
        $startDate = new DateTime($startDate);
        $difference = $startDate->diff(new DateTime($date));
        $minutes = $difference->days * 24 * 60;
        $minutes += $difference->h * 60;
        $minutes += $difference->i;
        return $minutes;
    }

}