<?php

namespace App\Controller;

class DueDateCalculatorValidator {

    /**
     * Decide the paramteres are proper (format and values)
     * @param starDate, the start date of issue
     * @param turnaround, the expected working hours on the issue
     * @return boolean, true if every paramters have valid format and paramters, otherwise false
     */
    public function calculateParamsAreValid($startDate, $turnaround) {

        $valid = true;

        if (empty($startDate) || empty($turnaround)) {
            $valid = false;
        }
        else if (!$this->_dateFormatIsValid($startDate)) {
            $valid = false;
        }
        else if (!$this->_tornaroundIsValid($turnaround)) {
            $valid = false;
        }

        return $valid;

    }

    /**
     * check the format of date (e.g. 2018-08-05 9:11:12)
     * @param starDate, the desired date
     * @return boolean
     */
    private function _dateFormatIsValid($startDate) {
        $valid = preg_match("/^\d{4}-\d{2}-\d{2} \d{1,2}:\d{1,2}(:\d{1,2})?$/",$startDate);
        return (boolean) $valid;
    }
 
    /**
     * Check the format and value of turnaround
     * @param turnaround, the desired working hours
     * @return boolean
     */
    private function _tornaroundIsValid($turnaround) {
        $valid = is_int($turnaround) && $turnaround > 0;
        return is_int($turnaround) && $turnaround > 0;
    }


    public function submitDateIsValid($submitDate,$startHour,$endHour) {
        $valid = true;
        
        if (!$this->_dateIsValid($submitDate)) {
            $valid = false;
        }
        else if (!$this->_timeIsValid($submitDate, $startHour, $endHour)) {
            $valid = false;
        }

        return $valid;
    }


    /**
     * Check the day is in the working days
     * @param date, the date to be examined
     * @return boolean
    */
    private function _dateIsValid($date) {
        $valid = false;
        $dayOfWeek = (int) date('N', strtotime($date));
        if ($dayOfWeek >= 1 && $dayOfWeek < 6) {
            $valid = true;
        }
        return $valid;
    }

    /**
     * Check the time is in the working hours
     * @param date, the date to be checked
     * @param startHour, the start working hour
     * @param endHour, the end working hour
     * @return boolean
    */
    private function _timeIsValid($date, $startHour, $endHour) {
        $valid = false;
        $hour = date("H",strtotime($date));
        if ($hour >= $startHour && $hour < $endHour) {
            $valid = true;
        }
        return $valid;
    }


}