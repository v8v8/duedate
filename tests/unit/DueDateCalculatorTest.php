<?php

class DueDateCalculatorTest extends \PHPUnit_Framework_TestCase {

    private $_calculator;

    public function setUp() {
        $this->_calculator = new \App\Controller\DueDateCalculator(9,17);
    }

    public function testInitDueDateCalculator() {

        $this->assertInstanceOf(\App\Controller\DueDateCalculator::class, $this->_calculator);

    }

    public function testGetStartAndEndWorkingHours() {

        $startWorkingHour = $this->_calculator->getStartWorkingHour();
        $endWorkingHour = $this->_calculator->getendWorkingHour();

        $this->assertEquals(9, $startWorkingHour);
        $this->assertNotEquals(17, $startWorkingHour);

        $this->assertEquals(17, $endWorkingHour);
        $this->assertNotEquals(9, $endWorkingHour);

    }

    /**
     * @expectedException \App\Exception\DueDateCalculatorException
     * @expectedExceptionMessage The parameters are not valid
     */

    public function testCallCalculateDueDateFunctionWithEmptyParams() {

        $submitDate = "";
        $turnaround = "";
        $this->_calculator->calculateDueDate($submitDate, $turnaround);

    }


    /**
     * @expectedException \App\Exception\DueDateCalculatorException
     * @expectedExceptionMessage The parameters are not valid
     */

    public function testCallCalculateDueDateFunctionWithWrongParams() {

        $submitDate = "2018-08-05";
        $turnaround = 0;
        $this->_calculator->calculateDueDate($submitDate, $turnaround);

    }

    /**
     * @expectedException \App\Exception\DueDateCalculatorException
     * @expectedExceptionMessage You can report issues only between 9AM and 5pm on Workdays!
     */

    public function testCallCalculateDueDateFunctionWithOutOfRangeDate() {

        $submitDate = "2018-08-05 9:11";
        $turnaround = 2;
        $this->_calculator->calculateDueDate($submitDate, $turnaround);

    }
    /**
     * @expectedException \App\Exception\DueDateCalculatorException
     * @expectedExceptionMessage You can report issues only between 9AM and 5pm on Workdays!
     */

    public function testCallCalculateDueDateFunctionWithOutOfRangeTime() {

        $submitDate = "2018-08-06 17:11";
        $turnaround = 2;
        $this->_calculator->calculateDueDate($submitDate, $turnaround);

    }

    public function testCalculateDateInOneDay() {

        $submitDate = "2018-08-06 11:11";
        $turnaround = 2;

        $dueDate = $this->_calculator->calculateDueDate($submitDate, $turnaround);

        $this->assertEquals("2018-08-06 13:11", $dueDate);
    }

    public function testCalculateDateTomorrow() {

        $submitDate = "2018-08-06 16:11";
        $turnaround = 2;

        $dueDate = $this->_calculator->calculateDueDate($submitDate, $turnaround);

        $this->assertEquals("2018-08-07 10:11", $dueDate);
    }


    public function testCalculateDateNextWeek() {

        $submitDate = "2018-08-10 16:11";
        $turnaround = 16;

        $dueDate = $this->_calculator->calculateDueDate($submitDate, $turnaround);

        $this->assertEquals("2018-08-14 16:11", $dueDate);
    }

    public function testCalculateDateNextYear() {

        $submitDate = "2018-12-31 10:55";
        $turnaround = 24;

        $dueDate = $this->_calculator->calculateDueDate($submitDate, $turnaround);

        $this->assertEquals("2019-01-03 10:55", $dueDate);
    }

}